﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using ColossalFramework;
using System.Diagnostics;
using ColossalFramework.Math;

// A lot of the information on how to do this came from Andrew Gotow's Unity3d Helicopter Tutorial
// http://andrewgotow.com/unity3d-helicopter-tutorial/

namespace CityCopter {
    public class HelicopterPhysics : MonoBehaviour {
        public GameObject helicopter = null; // must be set
        public GameObject mainRotor = null; // must be set
        public GameObject blurRotor = null; // must be set
        MeshRenderer mainRotorRenderer = null;
        MeshRenderer blurRotorRenderer = null;
        Rigidbody rigidbody = null;
        BoxCollider[] helicopterColliders = null;
        BoxCollider[] terrainColliders = null;
        public AudioSource audio = null;

        // ** Collisions **
        Vector3 lastPosition = Vector3.zero;
        TerrainManager terrainManager; 

        // Building collisions
        BuildingManager buildingManager;
        CircularBuffer<ColliderContainer> buildingColliders;
        Segment3[] buildingRays;
        HashSet<ushort> currentBuildings;
        GameObject buildingColliderContainer;

        // Road collisions
        NetManager netManager;
        CircularBuffer<ColliderContainer> roadColliders;
        HashSet<ushort> currentRoads;
        Config config;

        public bool slewMode = false;

        private float maxAlt = 0; // TODO: Make this dependent on helicopter
        // TODO: Currently maxAlt is reset to the current terrain height + 250 when resetPhysics is called (which means reset physics should be called at least once!)
        private const float MAX_DEFAULT_ALT = 250;
        public const int MAX_ALT_BUFFER = 40; 

        public const int NUM_RAYS = 200; // TODO Reduce?
        public const int NUM_BUILDING_MESHES = 30;
        public const int NUM_BUILDING_COLLIDERS = NUM_RAYS;
        // ** **

        private const string TERRAIN_COLLIDER_INTERNAL_NAME = CityCopter.Mod.MOD_NAME + "_TerrainCollider";
        private const string BUILDING_COLLIDER_INTERNAL_NAME = CityCopter.Mod.MOD_NAME + "_BuildingCollider";

        // ** movement stuff **
        float maxRotorVelocity = 9200f;// degrees per second
        float rotorRotation = 0.0f; 			// degrees... used for animating rotors
        //float max_tailRotorVelocity = 2200.0f; // degrees per second
        //float tail_Rotor_Rotation = 0.0f; // degrees... used for animating rotors

//        float maxRotorForce = 20241.11f;	// newtons
        float maxRotorForce = 17241.11f;	// newtons
        float rotorVelocity = 0.0f;			// value between 0 and 1

        float maxTailRotorForce = 32000.0f; // newtons
        float tailRotorVelocity = 0.0f; // value between 0 and 1

//        float forwardRotorTorqueMultiplier  = 3.0f;// multiplier for control input
        float forwardRotorTorqueMultiplier  = 2.0f;// multiplier for control input
//        float sidewaysRotorTorqueMultiplier = 0.2f;// multiplier for control input
        float sidewaysRotorTorqueMultiplier = 0.1f;// multiplier for control input

        bool mainRotorActive = true;
        bool tailRotorActive = true;


        Stopwatch s;
        void FixedUpdate() { 
            if (Vector3.Distance(transform.position, lastPosition) > 25) { 
                s = new Stopwatch();
                s.Start();
                lastPosition = transform.position;
                if (config.terrainCollisionDetection) { 
                    repositionTerrainColliders();
                }

                // TODO: Event-based notification of change?
                if (config.buildingCollisionDetection) { 
                    repositionBuildingBoxes();
                }
                s.Stop();
                //Debugger.Log("Collision detection took " + s.ElapsedMilliseconds + "ms to set up.");
            }
            if (slewMode) { 
                slewFixedUpdate();
            } else { 
                helicopterFixedUpdate();
            }
        }

        void Update() { 
            if (slewMode) { 
                slewUpdate();
            } else { 
                helicopterUpdate();
            }
        }


        public bool init(CityCopterController.HelicopterTypes helicopterType) { 
            bool succ = true;
            config = Config.Instance;

            if (helicopterType == CityCopterController.HelicopterTypes.N916MU) { 
                initN916MU();
            }
            initTerrainColliders();
            initBuildingColliders();
            initRoadColliders();

            return succ;
        }

        private void initBuildingColliders() { 
            buildingManager = Singleton<BuildingManager>.instance;
            currentBuildings = new HashSet<ushort>();
            buildingColliders = new CircularBuffer<ColliderContainer>(NUM_BUILDING_COLLIDERS);
            buildingColliderContainer = new GameObject();
            buildingColliderContainer.name = Mod.MOD_NAME + "_buildingColliders";
            for (int i=0; i<NUM_BUILDING_COLLIDERS; ++i) { 
                ColliderContainer cc = new ColliderContainer();
                cc.owner = new GameObject();
                cc.owner.transform.parent = buildingColliderContainer.transform;
                cc.meshCollider = cc.owner.AddComponent<MeshCollider>();
                buildingColliders.Insert(cc);
            }

            buildingRays = new Segment3[NUM_RAYS];
            for (int i=0; i<NUM_RAYS; ++i) { 
                buildingRays[i] = new Segment3();
            }
            
        }

        // A lot of this stuff is shared with building colliders
        private void initRoadColliders() { 
            netManager = Singleton<NetManager>.instance;
            currentRoads = new HashSet<ushort>();
            roadColliders = new CircularBuffer<ColliderContainer>(NUM_BUILDING_COLLIDERS);
            for (int i=0; i<NUM_BUILDING_COLLIDERS; ++i) { 
                ColliderContainer cc = new ColliderContainer();
                cc.owner = new GameObject();
                cc.owner.transform.parent = buildingColliderContainer.transform;
                cc.boxCollider = cc.owner.AddComponent<BoxCollider>();
                roadColliders.Insert(cc);
            }
        }

        private void initTerrainColliders() { 
            terrainManager = Singleton<TerrainManager>.instance;

            terrainColliders = new BoxCollider[4];
            for (int x=0; x<terrainColliders.Length; ++x) { 
                Debugger.Log("Adding a new terrain collider");
                terrainColliders[x] = terrainManager.gameObject.AddComponent<BoxCollider>();
                terrainColliders[x].name = TERRAIN_COLLIDER_INTERNAL_NAME + "_" + x;
            }
        }

        void repositionBuildingBoxes() { 
            Building building;
            NetNode node;
            BuildingInfo info;
            NetSegment netSegment;
            ushort buildingIndex = 0;
            ushort nodeIndex = 0;
            ushort segmentIndex = 0;
            Vector3 hitPos = Vector3.zero;

            // Spherical raycast
            Segment3 ray;
            float radius = 40f;
            float x,y,z, phi;
            currentBuildings.Clear();
            for (int n=0; n<NUM_RAYS; n++) { 
                z = UnityEngine.Random.Range(-radius, radius);
                phi = UnityEngine.Random.Range(0, Mathf.PI*2);
                x = Mathf.Sqrt(radius*radius - z*z)*Mathf.Cos(phi);
                y = Mathf.Sqrt(radius*radius - z*z)*Mathf.Sin(phi);

                ray = new Segment3(transform.position, new Vector3(x,y,z) + transform.position);

                buildingManager.RayCast(ray, ItemClass.Service.None, ItemClass.SubService.None, ItemClass.Layer.Default, Building.Flags.None, out hitPos, out buildingIndex);
                if (hitPos != Vector3.zero) { 
                    building = buildingManager.m_buildings.m_buffer[buildingIndex];
                    info = building.Info;

                    if (!currentBuildings.Contains(buildingIndex)) { 
                        //Debugger.Log("Creating a mesh for building " + buildingIndex + ", name: " + info.name);
                        Mesh m = info.m_mesh;
                        ColliderContainer cc = buildingColliders.getCurrentAndIncrement();
                        cc.meshCollider.sharedMesh = info.m_mesh;
                        cc.owner.transform.position = building.m_position;
                        cc.owner.transform.Rotate(Vector3.up, building.m_angle);
                        //Debugger.Log("Done creating a mesh.");
                        currentBuildings.Add(buildingIndex);
                    }
                }

               netManager.RayCast(ray, 0f, ItemClass.Service.Road, ItemClass.Service.PublicTransport, ItemClass.SubService.None, ItemClass.SubService.None, ItemClass.Layer.Default, ItemClass.Layer.None, NetNode.Flags.None, NetSegment.Flags.None, out hitPos, out nodeIndex, out segmentIndex);  
                if (hitPos != Vector3.zero) { 
                    node = netManager.m_nodes.m_buffer[nodeIndex];
                    netSegment = netManager.m_segments.m_buffer[segmentIndex];

                    if (!currentRoads.Contains(segmentIndex)) { 
                        ColliderContainer cc = roadColliders.getCurrentAndIncrement();
                        // TODO: snap to ground better
                        cc.boxCollider.center = netSegment.m_bounds.center - new Vector3(0, 2f, 0);
                        cc.boxCollider.size = new Vector3(netSegment.m_bounds.size.x, 1.0f, netSegment.m_bounds.size.z);
                        cc.boxCollider.transform.rotation = netSegment.Info.transform.rotation;
                        currentRoads.Add(segmentIndex);
                    }
                }
            }

        }

        private void repositionTerrainColliders() { 
            float x1, x2, z1, z2, y;
            x1 = transform.position.x + 25;
            x2 = transform.position.x - 25;
            z1 = transform.position.z + 25;
            z2 = transform.position.z - 25;
            y = terrainManager.SampleDetailHeight(transform.position);

            terrainColliders[0].center = new Vector3(transform.position.x, y, transform.position.z);
            terrainColliders[0].size = new Vector3(50f, 0.5f, 50f);

        }

        private bool initN916MU() { 
            bool succ = true;

            mainRotorRenderer = mainRotor.GetComponent<MeshRenderer>();
            blurRotorRenderer = blurRotor.GetComponent<MeshRenderer>();

            rigidbody = gameObject.AddComponent<Rigidbody>();
            rigidbody.mass = 1276;
            rigidbody.drag = 0.1f;
            rigidbody.angularDrag = 1.5f;
            rigidbody.useGravity = true;
            rigidbody.isKinematic = false;
            rigidbody.interpolation = RigidbodyInterpolation.None;
            rigidbody.collisionDetectionMode = CollisionDetectionMode.Discrete;

            // Helicopter colliders
            helicopterColliders = new BoxCollider[3];
            helicopterColliders[0] = helicopter.AddComponent<BoxCollider>();
            helicopterColliders[0].center = new Vector3(0, 1.4915f, 0.661f);
            helicopterColliders[0].size = new Vector3(1.428f, 1.911f, 4.699f);
            helicopterColliders[0].enabled = true;

            helicopterColliders[1] = helicopter.AddComponent<BoxCollider>();
            helicopterColliders[1].center = new Vector3(0, 1.6419f, -4.0278f);
            helicopterColliders[1].size = new Vector3(0.74f, 1.08f, 5.8f);
            helicopterColliders[1].enabled = true;

            helicopterColliders[2] = helicopter.AddComponent<BoxCollider>();
            helicopterColliders[2].center = new Vector3(0f, 0.4352f, 0.395f);
            helicopterColliders[2].size = new Vector3(2.4f, 0.74f, 2.84f);
            helicopterColliders[2].enabled = true;
            //positionDebugMarkers(helicopterColliders[2]);

            return succ;
        }
        
        GameObject[] markers = null;
        public void positionDebugMarkers(BoxCollider collider) { 
            if (markers == null) {
                markers = new GameObject[8];
                Material m = new Material(Shader.Find("Diffuse"));
                for (int x=0; x<markers.Length; ++x) { 
                    markers[x] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    markers[x].GetComponent<Renderer>().sharedMaterial = m;
                    markers[x].transform.localScale = new Vector3(0.5f,0.5f,0.5f);
                    markers[x].transform.position = transform.position;
                    markers[x].transform.parent = transform;
                    markers[x].transform.position = collider.center;
                }
            }

            markers[0].transform.Translate(new Vector3(collider.size.x/2, collider.size.y/2, collider.size.z/2));
            markers[1].transform.Translate(new Vector3(collider.size.x/2, collider.size.y/2, -collider.size.z/2));
            markers[2].transform.Translate(new Vector3(collider.size.x/2, -collider.size.y/2, collider.size.z/2));
            markers[3].transform.Translate(new Vector3(collider.size.x/2, -collider.size.y/2, -collider.size.z/2));
            markers[4].transform.Translate(new Vector3(-collider.size.x/2, collider.size.y/2, collider.size.z/2));
            markers[5].transform.Translate(new Vector3(-collider.size.x/2, collider.size.y/2, -collider.size.z/2));
            markers[6].transform.Translate(new Vector3(-collider.size.x/2, -collider.size.y/2, collider.size.z/2));
            markers[7].transform.Translate(new Vector3(-collider.size.x/2, -collider.size.y/2, -collider.size.z/2));
        }


        public void disablePhysics() { 
            rigidbody.isKinematic = true;
            rigidbody.detectCollisions = false;
        }

        public void enablePhysics() { 
            rigidbody.isKinematic = false;
            rigidbody.detectCollisions = true;
        }

        public void shutdown() { 
            if (terrainColliders != null) { 
                for (int x=0; x<terrainColliders.Length; ++x) { 
                    Destroy(terrainColliders[x]);
                }

                ColliderContainer[] array = buildingColliders.ToArray();
                for (int x=0; x<array.Length; ++x) { 
                    Destroy(array[x].meshCollider);
                    Destroy(array[x].owner);
                }

                array = roadColliders.ToArray();
                for (int x=0; x<array.Length; ++x) { 
                    Destroy(array[x].boxCollider);
                    Destroy(array[x].owner);
                }
            }
            Destroy(rigidbody);
        }

        private void slewFixedUpdate() { 

        }

        private void slewUpdate() {

        } 


        private void helicopterUpdate() { 
            if (audio) { 
                audio.pitch = rotorVelocity;
            }

            if (mainRotorActive) { 
                mainRotor.transform.rotation = transform.rotation * Quaternion.Euler(0, rotorRotation, 0);
            }

            rotorRotation += maxRotorVelocity * rotorVelocity * Time.deltaTime;

            float hoverRotorVelocity = (rigidbody.mass * Mathf.Abs(Physics.gravity.y) / maxRotorForce);
            float hoverTailRotorVelocity = (maxRotorForce * rotorVelocity) / maxTailRotorForce;

            if (Input.GetKey(KeyCode.W)) {
                rotorVelocity += 0.005f;
            } else if (Input.GetKey(KeyCode.S)) {
                rotorVelocity -= 0.010f;
            } else { 
                rotorVelocity = Mathf.Lerp(rotorVelocity, hoverRotorVelocity, Time.deltaTime * Time.deltaTime * 5);
            }

/*            float yawInput = 0.0f;

            if (Input.GetKey(KeyCode.LeftArrow)) { 
                yawInput = -1.0f;
            } else if (Input.GetKey(KeyCode.RightArrow)) { 
                yawInput = 1.0f;
            }

            tailRotorVelocity = hoverTailRotorVelocity - yawInput;
            */

            tailRotorVelocity = (Event.current.modifiers != Config.Instance.mouseLookModifierKey) ? (hoverTailRotorVelocity - Input.GetAxis("Mouse X")) : hoverTailRotorVelocity;
            if (rotorVelocity > 1.0f) { 
                rotorVelocity = 1.0f;
            } else if ( rotorVelocity < 0.05f) { 
                rotorVelocity = 0.05f;
            }

        }

        public void setHover() { 
            float hoverRotorVelocity = (rigidbody.mass * Mathf.Abs(Physics.gravity.y) / maxRotorForce);
            float hoverTailRotorVelocity = (maxRotorForce * rotorVelocity) / maxTailRotorForce;

            rotorVelocity = hoverRotorVelocity;
            tailRotorVelocity = hoverTailRotorVelocity;
        }

        public void resetPhysics() { 
            rotorRotation = 0;
            rotorVelocity = 0;
            tailRotorVelocity = 0;
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            maxAlt = MAX_DEFAULT_ALT + terrainManager.SampleDetailHeight(transform.position);
        }

        public void setMaxAlt(int maxAlt) { 
            this.maxAlt = maxAlt;
        }

        void helicopterFixedUpdate() { 
            Vector3 torqueValue = new Vector3();
/*            float pitchControl = 0.0f;
            if (Input.GetKey(KeyCode.UpArrow)) { 
                pitchControl = 1.0f;
            } else if (Input.GetKey(KeyCode.DownArrow)) { 
                pitchControl = -1.0f;
            }

            */
            float pitchControl = (Event.current.modifiers != Config.Instance.mouseLookModifierKey) ? -Input.GetAxis("Mouse Y") : 0f;
            if (Config.Instance.invertYAxis) { 
                pitchControl *= -1;
            }

            float rollControl = 0.0f;
            if (Input.GetKey(KeyCode.D)) { 
                rollControl = 1.0f;
            } else if (Input.GetKey(KeyCode.A)) { 
                rollControl = -1.0f;
            }

            Vector3 controlTorque = new Vector3(pitchControl * forwardRotorTorqueMultiplier, 
                1.0f, 
                -rollControl * sidewaysRotorTorqueMultiplier);

            if (mainRotorActive) { 
                torqueValue += (controlTorque * maxRotorForce * rotorVelocity);
                rigidbody.AddRelativeForce(Vector3.up * maxRotorForce * rotorVelocity);

                if (Vector3.Angle(Vector3.up, transform.up) < 80 && Config.Instance.flightSafeguards) { 
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0), Time.deltaTime * rotorVelocity * 2);
                }
            }

            if (Config.Instance.enforceAltitudeLimit && transform.position.y > maxAlt) { 
                float amount; //= transform.position.y - MAX_ALT
                if (transform.position.y > maxAlt + MAX_ALT_BUFFER) { 
                    amount = 1.0f;
                } else { 
                    amount = (transform.position.y - maxAlt) / MAX_ALT_BUFFER;
                }
                //Debugger.Log("Applying force modifier of: " + amount + ". Total force magnitude: " + maxRotorForce * amount);
                rigidbody.AddRelativeForce(Vector3.down * maxRotorForce * amount);
            }

            if (tailRotorActive) { 
                torqueValue -= (Vector3.up * maxTailRotorForce * tailRotorVelocity);
            }

            // TODO: make this cleaner
            if (blurRotor != null && blurRotorRenderer != null) { 
                if (rotorVelocity > 0.2f) { 
                    mainRotorRenderer.enabled = false;
                    blurRotorRenderer.enabled = true;
                } else {
                    mainRotorRenderer.enabled = true;
                    blurRotorRenderer.enabled = false;
                }
            }
            /*float angleZ = transform.eulerAngles.z;
//            angleZ = Mathf.Clamp(angleZ, 90, 270);
            if (angleZ > 180 && angleZ < 300) { 
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 330), Time.deltaTime * rotorVelocity);
                //angleZ = 300;
            } else if (angleZ > 60 && angleZ < 300) { 
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 30), Time.deltaTime * rotorVelocity);
            }
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, angleZ);
            */

            

            rigidbody.AddRelativeTorque(torqueValue);
        }


    }
}
