﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColossalFramework.UI;
using UnityEngine;


// Three Make* functions from FPS Camera

namespace CityCopter {
    public class UIFactory {
        public delegate void SliderSetValue(float value);
        public delegate void ButtonClicked();
        public static UISlider MakeSlider(UIPanel panel, string name, string text, float y, float value, float min, float max, SliderSetValue setValue) { 
            var label = panel.AddUIComponent<UILabel>();
            label.name = name + "Label";
            label.text = text;
            label.relativePosition = new Vector3(4.0f, y);
            label.textScale = 0.8f;

            var slider = panel.AddUIComponent<UISlider>();
            slider.name = name + "Slider";
            slider.minValue = min;
            slider.maxValue = max;
            slider.stepSize = 0.25f;
            slider.value = value;
            slider.relativePosition = new Vector3(200.0f, y);
            slider.size = new Vector2(158.0f, 16.0f);

            var thumbSprite = slider.AddUIComponent<UISprite>();
            thumbSprite.name = "Thumb";
            thumbSprite.spriteName = "SliderBudget";

            slider.backgroundSprite = "ScrollbarTrack";
            slider.thumbObject = thumbSprite;
            slider.orientation = UIOrientation.Horizontal;
            slider.isVisible = true;
            slider.enabled = true;
            slider.canFocus = true;
            slider.isInteractive = true;

            var valueLabel = panel.AddUIComponent<UILabel>();
            valueLabel.name = name + "ValueLabel";
            valueLabel.text = slider.value.ToString("0.00");
            valueLabel.relativePosition = new Vector3(362.0f, y);
            valueLabel.textScale = 0.8f;

            slider.eventValueChanged += (component, f) =>
            {
                setValue(f);
                valueLabel.text = slider.value.ToString("0.00");
            };

            return slider;
        }

        public static UIButton MakeButton(UIPanel panel, string name, string text, float y, ButtonClicked clicked) { 
            var button = panel.AddUIComponent<UIButton>();
            button.name = name;
            button.text = text;
            button.relativePosition = new Vector3(200.0f, y - 6.0f);
            button.size = new Vector2(100.0f, 24.0f);
            button.normalBgSprite = "ButtonMenu";
            button.disabledBgSprite = "ButtonMenuDisabled";
            button.hoveredBgSprite = "ButtonMenuHovered";
            button.focusedBgSprite = "ButtonMenu";
            button.pressedBgSprite = "ButtonMenuPressed";
            button.eventClick += (component, param) =>
            {
                clicked();
            };

            return button;
        }

        public delegate void CheckboxSetValue(bool value);
        public static UICheckBox MakeCheckbox(UIPanel panel, string name, string text, float y, bool value, CheckboxSetValue setValue) { 
            var label = panel.AddUIComponent<UILabel>();
            label.name = name;
            label.text = text;
            label.relativePosition = new Vector3(4.0f, y);
            label.textScale = 0.8f;

            var checkbox = panel.AddUIComponent<UICheckBox>();
            checkbox.AlignTo(label, UIAlignAnchor.TopLeft);
            checkbox.relativePosition = new Vector3(checkbox.relativePosition.x + 332.0f, checkbox.relativePosition.y - 6.0f);
            checkbox.size = new Vector2(20.0f, 20.0f);
            checkbox.isVisible = true;
            checkbox.canFocus = true;
            checkbox.isInteractive = true;

            checkbox.eventCheckChanged += (component, newValue) =>
            {
                setValue(newValue);
            };

            var uncheckSprite = checkbox.AddUIComponent<UISprite>();
            uncheckSprite.size = new Vector2(20.0f, 20.0f);
            uncheckSprite.relativePosition = new Vector3(0, 0);
            uncheckSprite.spriteName = "check-unchecked";
            uncheckSprite.isVisible = true;

            var checkSprite = checkbox.AddUIComponent<UISprite>();
            checkSprite.size = new Vector2(20.0f, 20.0f);
            checkSprite.relativePosition = new Vector3(0, 0);
            checkSprite.spriteName = "check-checked";

            checkbox.isChecked = value;
            checkbox.checkedBoxObject = checkSprite;
            return checkbox;
        }

        public static UILabel MakeUILabel(UIPanel panel, string name, string text, bool autoSize, Vector2 size, UIHorizontalAlignment textAlignment) { 
            UILabel label = panel.AddUIComponent<UILabel>();

            label.name = name;
            label.text = text;
            label.autoSize = autoSize;
            label.size = size;
            label.textAlignment = textAlignment;

            return label;
        }

        public static UILabel MakeUILabel(UIPanel panel, string name, string text, bool autoSize, Vector2 size, UIHorizontalAlignment textAlignment, UIComponent alignTarget, UIAlignAnchor alignAnchor) { 
            UILabel label = MakeUILabel(panel, name, text, autoSize, size, textAlignment);
            label.AlignTo(alignTarget, alignAnchor);

            return label;
        }


    }
}
