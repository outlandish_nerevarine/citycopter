﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using ColossalFramework.UI;

// Learned how to do this UI stuff from FPS Camera

namespace CityCopter {
    public enum UIState { UI_STATE_NORMAL, UI_STATE_WAIT_FOR_MAIN_HOTKEY };

    public class CityCopterUI : MonoBehaviour {
        private static CityCopterUI _instance;

        public UIState uiState = UIState.UI_STATE_NORMAL;
        private UIPanel panel;
        private UIButton configButton;
        private UIButton hotkeyToggleButton;
        public bool uiVisible = false;

        // For cancelling key programming 
        private EventModifiers originalModifier;
        private KeyCode originalKey;

        public static CityCopterUI Instance { 
            get { 
                if (_instance == null) { 
                    _instance = ModPackage.instance.gameObject.AddComponent<CityCopterUI>();
                }
                return _instance;
            }
        }

        private CityCopterUI() { 
            UIView uiView = FindObjectOfType<UIView>();
            UIComponent fullscreenContainer = uiView.FindUIComponent("FullScreenContainer");


            configButton = uiView.AddUIComponent(typeof(UIButton)) as UIButton;
            configButton.name = "CityCopterConfigurationUIButton";
            configButton.gameObject.name = "CityCopterConfigurationUIButton";
            configButton.width = 46;
            configButton.height = 46;

            configButton.pressedBgSprite = "RoundBackBigPressed";
            configButton.normalBgSprite = "RoundBackBig";
            configButton.hoveredBgSprite = "RoundBackBigHovered";
            configButton.disabledBgSprite = "RoundBackBigDisabled";

            configButton.normalFgSprite = "Options";
            configButton.foregroundSpriteMode = UIForegroundSpriteMode.Scale;
            configButton.scaleFactor = 0.79f;

            configButton.tooltip = "CityCopter configuration";
            configButton.tooltipBox = uiView.defaultTooltipBox;
            configButton.enabled = false;
            configButton.color = new Color(0f,1.0f,0f);
            configButton.pressedColor = new Color(0f, 1.0f, 0f);
            configButton.hoveredColor = new Color(0f, 1.0f, 0f);
            configButton.enabled = true;

            UIComponent anchorButton = uiView.FindUIComponent("Esc");
            configButton.relativePosition = new Vector2
            (
                anchorButton.relativePosition.x + anchorButton.width / 2.0f - configButton.width / 2.0f,
                anchorButton.relativePosition.y + anchorButton.height / 2.0f - configButton.height / 2.0f + anchorButton.height + 8.0f
            );

            configButton.eventClick += (component, param) => { if (uiVisible) Hide(); else Show(); };

            panel = fullscreenContainer.AddUIComponent<UIPanel>();
            panel.size = new Vector2(400, 750);
            panel.backgroundSprite = "SubcategoriesPanel";
            panel.relativePosition = new Vector3(configButton.relativePosition.x - panel.size.x, configButton.relativePosition.y + 60.0f);
            panel.name = "CityCopter configuration";

            createPanelLayout();

            Hide();
        }

        private void createPanelLayout() { 
            float y = 0.0f;
            CityCopterController cityCopter = ModPackage.instance.cityCopter;

            UILabel titleLabel = UIFactory.MakeUILabel(panel, "Title", Mod.MOD_NAME + " " + Mod.MOD_VERSION + " Configuration", false, new Vector2(panel.size.x, 24.0f), UIHorizontalAlignment.Center, panel, UIAlignAnchor.TopLeft);
            titleLabel.relativePosition = new Vector3(titleLabel.relativePosition.x, titleLabel.relativePosition.y + 2.0f);
            y += 28.0f;

            UILabel reminderLabel = UIFactory.MakeUILabel(panel, "Reminder", "(Don't forget to build a hangar!)", false, new Vector2(panel.size.x, 24.0f), UIHorizontalAlignment.Center, panel, UIAlignAnchor.TopLeft);
            reminderLabel.relativePosition = new Vector3(reminderLabel.relativePosition.x, y);

            y += 48.0f;

            UIFactory.MakeSlider(panel, "FieldOfView", "Field of View", y,
                Config.Instance.fieldOfView, 30.0f, 120.0f,
                value =>
                {
                    cityCopter.setFieldOfView(value);
                });

            y += 28.0f;
            
            UILabel hotkeyToggleLabel = panel.AddUIComponent<UILabel>();
            hotkeyToggleLabel.name = "ToggleModeLabel";
            hotkeyToggleLabel.text = "Hotkey to Enter Helicopter";
            hotkeyToggleLabel.relativePosition = new Vector3(4.0f, y);
            hotkeyToggleLabel.textScale = 0.8f;

            hotkeyToggleButton = UIFactory.MakeButton(panel, "ToggleModeButton",
                Config.Instance.toggleHelicopterFlightKeyModifiers.ToString() + " + " + Config.Instance.toggleHelicopterFlightKey.ToString(), y,
                () =>
                {
                    if (uiState == UIState.UI_STATE_NORMAL)
                    {
                        originalKey = Config.Instance.toggleHelicopterFlightKey;
                        originalModifier = Config.Instance.toggleHelicopterFlightKeyModifiers;
                        uiState = UIState.UI_STATE_WAIT_FOR_MAIN_HOTKEY;
                        hotkeyToggleButton.text = "Waiting";
                    }
                });
            hotkeyToggleButton.width = 180f;

            y += 28.0f + 8.0f;

            UIFactory.MakeCheckbox(panel, "BuildingCollisionDetection", "Building Collision Detection (high performance)", y, Config.Instance.buildingCollisionDetection,
                value =>
                {
                    Config.Instance.buildingCollisionDetection = value;
                    Config.Instance.Save();
                });

            y += 28.0f + 8.0f;

            UIFactory.MakeCheckbox(panel, "TerrainCollisionDetection", "Terrain Collision Detection (you probably want this)", y, Config.Instance.terrainCollisionDetection,
                value =>
                {
                    Config.Instance.terrainCollisionDetection = value;
                    Config.Instance.Save();
                });

            y += 28.0f + 8.0f;

/*            UIFactory.MakeCheckbox(panel, "LockMouseCursor", "Lock Mouse Cursor", y, Config.Instance.lockMouse,
                value =>
                {
                    Config.Instance.lockMouse = value;
                    Config.Instance.Save();
                });

            y += 28.0f + 8.0f;

            UIFactory.MakeCheckbox(panel, "UseMouselook", "Use Mouselook", y, Config.Instance.mouseLook,
                value =>
                {
                    Config.Instance.mouseLook = value;
                    Config.Instance.Save();
                });

            y += 28.0f + 8.0f;
            */

            UIFactory.MakeCheckbox(panel, "InvertY", "Invert Y Axis", y, Config.Instance.invertYAxis,
                value =>
                {
                    Config.Instance.invertYAxis = value;
                    Config.Instance.Save();
                });

            y += 28.0f + 8.0f;

            UIFactory.MakeCheckbox(panel, "FlightSafeguards", "Easier Flight", y, Config.Instance.flightSafeguards,
                value =>
                {
                    Config.Instance.flightSafeguards = value;
                    Config.Instance.Save();
                });

            y += 28.0f + 8.0f;

            UIFactory.MakeCheckbox(panel, "DebugConsole", "Enable Logging", y, Config.Instance.debugConsoleEnabled,
                value =>
                {
                    Config.Instance.debugConsoleEnabled = value;
                    Config.Instance.Save();
                });

            y += 28.0f + 8.0f;

            UIFactory.MakeCheckbox(panel, "Spotlight", "Use Spotlight", y, Config.Instance.useSpotlight,
                value =>
                {
                    Config.Instance.useSpotlight = value;
                    Config.Instance.Save();
                    cityCopter.SetSpotlight(value);
                });

            y += 28.0f + 8.0f;

            UIFactory.MakeCheckbox(panel, "NightMode", "Night mode", y, Config.Instance.nightMode,
                value =>
                {
                    Config.Instance.nightMode = value;
                    Config.Instance.Save();
                    if (cityCopter.modeEnabled) { 
                        cityCopter.SetNightMode(value);
                    }
                });

            y += 28.0f + 8.0f;

            UIFactory.MakeCheckbox(panel, "DebugKeys", "Use Debug Keys", y, Config.Instance.useDebugKeys,
                value =>
                {
                    Config.Instance.useDebugKeys = value;
                    Config.Instance.Save();
                });

            y += 28.0f + 8.0f;

            UIFactory.MakeCheckbox(panel, "AltitudeLimit", "Enforce Altitude Limit", y, Config.Instance.enforceAltitudeLimit,
                value =>
                {
                    Config.Instance.enforceAltitudeLimit = value;
                    Config.Instance.Save();
                });

            y += 28.0f + 8.0f;

            UILabel expLabel = UIFactory.MakeUILabel(panel, "PingExplanation", "Sends an anonymous ping when the mod starts\nThis helps me with usage and version info!", false, new Vector2(panel.size.x, 40f), UIHorizontalAlignment.Left);
            expLabel.relativePosition = new Vector3(5, y);
            y += 39.0f;

            UIFactory.MakeCheckbox(panel, "AnonPing", "Anonymous Ping on Start-up", y, Config.Instance.anonPingServerOnStart,
                value =>
                {
                    Config.Instance.anonPingServerOnStart = value;
                    Config.Instance.Save();
                });

            y += 28.0f + 16.0f;

            UILabel directionsLabel = UIFactory.MakeUILabel(panel, "Directions", "Mouse - Pitch and yaw\nWASD - Collective and roll\nMouse + Shift - Look around", false, new Vector2(panel.size.x, 80f), UIHorizontalAlignment.Left);
            directionsLabel.relativePosition = new Vector3(5, y);

            y += 90.0f;

            UILabel websiteLabel = UIFactory.MakeUILabel(panel, "Website", "By /u/InconsolableCellist\nTemp model by edson\nhttp://www.reddit.com/r/CityCopter\n@MobiusStripShow", false, new Vector2(panel.size.x, 70f), UIHorizontalAlignment.Left);
            websiteLabel.relativePosition = new Vector3(5, y);
            y += 49.0f;



        }




        private static bool isModifier(KeyCode kc) {
            bool modifier = false;

            if (kc == KeyCode.LeftAlt || kc ==  KeyCode.LeftApple || kc ==  KeyCode.LeftCommand || kc ==  KeyCode.LeftControl || kc ==  KeyCode.LeftShift || kc ==  KeyCode.LeftAlt || 
                kc == KeyCode.RightAlt || kc ==  KeyCode.RightApple || kc ==  KeyCode.RightCommand || kc ==  KeyCode.RightControl || kc ==  KeyCode.RightShift || kc ==  KeyCode.RightAlt) { 
                modifier = true;
            }

            return modifier;
        }


        private void OnGUI() { 
            if (uiState == UIState.UI_STATE_NORMAL || !uiVisible) return;

            if (uiState == UIState.UI_STATE_WAIT_FOR_MAIN_HOTKEY) { 
                if (Event.current.isKey) { 
                    // Cancel
                    if (Event.current.keyCode == KeyCode.Escape) { 
                        Config.Instance.toggleHelicopterFlightKeyModifiers = originalModifier;
                        Config.Instance.toggleHelicopterFlightKey = originalKey;
                        hotkeyToggleButton.text = Config.Instance.toggleHelicopterFlightKeyModifiers.ToString() + " + " + Config.Instance.toggleHelicopterFlightKey.ToString();
                        Event.current.Use();
                        uiState = UIState.UI_STATE_NORMAL;
                        return;
                    }

                    if (!isModifier(Event.current.keyCode)) { 
                        KeyCode keycode = Event.current.keyCode;
                        Config.Instance.toggleHelicopterFlightKey = keycode;
                        Config.Instance.toggleHelicopterFlightKeyModifiers = Event.current.modifiers;
                        Config.Instance.Save();
                        hotkeyToggleButton.text = Event.current.modifiers.ToString() + " + " + keycode.ToString();
                        Event.current.Use();
                        uiState = UIState.UI_STATE_NORMAL;
                    }
                } // event.current.type
            } // UIState
        }

        //TODO: Unloading and destroying

        public void Show() { 
            uiVisible = true;
            panel.isVisible = true;
            // Workaround for color not being respected
            configButton.enabled = false;
            configButton.enabled = true;
            uiState = UIState.UI_STATE_NORMAL;
        }

        public void Hide() { 
            uiVisible = false;
            panel.isVisible = false;
            // Workaround for color not being respected
            configButton.enabled = false;
            configButton.enabled = true;
        }

    }
}
