﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace CityCopter {
    class ResourceLoader {
        public static byte[] ExtractResource(String filename, bool verbose = false)
        {
            System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            if (verbose) {
                string[] resources = a.GetManifestResourceNames();
                foreach (string s in resources) { 
                    Debugger.Log("+ The resource loader sees: " + s);
                }
            }

            using (Stream resFilestream = a.GetManifestResourceStream(filename))
            {
                if (resFilestream == null) return null;
                byte[] ba = new byte[resFilestream.Length];
                resFilestream.Read(ba, 0, ba.Length);
                return ba;
            }
        }

        public static bool SaveResourceToDisk(Byte[] bytes, String absolutePath) { 
            bool succ = true;
            try { 
                File.WriteAllBytes(absolutePath, bytes);
            } catch { 
                succ = false;
            }
            return succ;
        }

        public static void WriteEmbeddedAssetToDisk(string embeddedAssetName, string pathName) { 
            Debugger.Log("Checking for " + pathName);
            if (!File.Exists(pathName)) { 
                Debugger.Log("    Doesn't exist! Extracting resource from DLL: " + embeddedAssetName);
                Byte[] oggBytes = ResourceLoader.ExtractResource(embeddedAssetName);
                if (oggBytes != null && oggBytes.Length > 0) {
                    Debugger.Log("    Resource extracted successfully. Saving it to " + pathName);
                    if (!ResourceLoader.SaveResourceToDisk(oggBytes, pathName)) { 
                        Debugger.Log("     Fatal error while trying to save the OGG to disk!");
                    } else { 
                        Debugger.Log("     Success.");
                    }
                } else { 
                    Debugger.Log("     Unable to extract resource!");
                }
            }

        }
    }
}
