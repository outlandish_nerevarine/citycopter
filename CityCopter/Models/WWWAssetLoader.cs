﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Reflection;

namespace CityCopter {
    class WWWAssetLoader : MonoBehaviour {
        public Texture2D texture; 
        public IEnumerator LoadTexture(string webURL, int width, int height) { 
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resources = asm.GetManifestResourceNames();
            foreach (string s in resources) { 
                Debugger.Log("I see: " + s);
            }
            Debugger.Log("Done parsing resources.");
            FileStream file = asm.GetFile("CityCopter_rotor_blur_512.png");
            yield return null;

        }

/*            texture = new Texture2D(width, height, TextureFormat.DXT1, false);
            Debugger.Log("Trying to load " + webURL);
            WWW www = new WWW(webURL);
            while (!www.isDone) {
               Debugger.Log("Yielding!");
               yield return null;
            }

            if (www.error == null) {
                www.LoadImageIntoTexture(texture);
                Debugger.Log("Finished loading attempt. Pixel length: " + texture.GetPixels().Length);
            } else { 
                Debugger.Log("Encountered an error when trying to load asset " + webURL + "!");
            }
        }

            */
    }
}
