﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using ColossalFramework.Packaging;

namespace CityCopter {
    class CustomModelLoader {
        public static GameObject LoadProp(string assetName, out bool succ, GameObject model = null) { 
            succ = false;
        // TODO : Remove
        /*PropInfo p;
        ushort prop;
        ColossalFramework.Math.Randomizer  r;
        PropInstance pInst;
                PropProperties pp;
                InfoManager infoManager;
                PropInfoGen pig;
                PropInfo pi;
        PropManager propManager;
        GameObject derp;
        HashSet<string> seenIt;*/

/*                PropManager.instance.m_props.m_buffer[0].Position = pilot.transform.position;
                r = new ColossalFramework.Math.Randomizer(80);
                prop = 0;
                p = new PropInfo();
                propManager = Singleton<PropManager>.instance;
                Debugger.Log("name: " + propManager.m_props.m_buffer[0].Info.name);
                Debugger.Log("name: " + propManager.m_props.m_buffer[400].Info.name);
                propManager.CreateProp(out prop, ref r, p, pilot.transform.position, 0f, true);
                Debugger.Log("prop is " + prop + ". size is now: " + propManager.m_props.m_size);
                pInst = propManager.m_props.m_buffer[prop];
                ushort infoindex = pInst.m_infoIndex;
                pp = propManager.m_properties;
                infoManager = Singleton<InfoManager>.instance;
                pig = new PropInfoGen();
                pi = Singleton<PropInfo>.instance;
                p = pInst.Info;
                p.gameObject.transform.position = pilot.transform.position;
                seenIt = new HashSet<string>();
                Debugger.Log("Length is: " + propManager.m_props.m_buffer.Length);
                for (int x=0; x<propManager.m_props.m_buffer.Length; ++x) { 
                    string name = propManager.m_props.m_buffer[x].Info.name;
                    if (!seenIt.Contains(name)) { 
                        seenIt.Add(name);
                        Debugger.Log("I see: " + name);
                    }
                }

                BuildingManager buildingManager = Singleton<BuildingManager>.instance;

                seenIt = new HashSet<string>();
                Debugger.Log("Length is now: " + buildingManager.m_buildings.m_buffer.Length);
                for (int x=0; x<buildingManager.m_buildings.m_buffer.Length; ++x) { 
                    string name = buildingManager.m_buildings.m_buffer[x].Info.name;
                    if (!seenIt.Contains(name)) { 
                        seenIt.Add(name);
                        Debugger.Log("I see: " + name);
                        if (name.Contains("watertower1")) { 
                            Debugger.Log("I think I'm on the watertower. Here are its props...");
                            BuildingInfo.Prop[] props = buildingManager.m_buildings.m_buffer[x].Info.m_props;
                            foreach (BuildingInfo.Prop theProp in buildingManager.m_buildings.m_buffer[x].Info.m_props) {
                                Debugger.Log("Prop name is: " + theProp.m_prop.name);
                                if (theProp.m_prop.name.Contains("Rustic Picnic Table")) { 
                                    Debugger.Log("Trying to spawn a picnic table now");
                                    derp = new GameObject();
                                    derp.transform.position = pilot.transform.position;
                                    derp.transform.rotation = pilot.transform.rotation;
                                    MeshFilter mf = derp.AddComponent<MeshFilter>();
                                    MeshRenderer mr = derp.AddComponent<MeshRenderer>();
                                    mf.sharedMesh = theProp.m_prop.m_mesh;
                                    if (mf.mesh != null) { 
                                        Debugger.Log("I was able to create the picnic table mesh!");
                                    }
                                    mr.sharedMaterial = theProp.m_prop.m_material;
                                }
                            }
                        }
                    }
                }
                Debugger.Log("pInst position: " + pInst.Position.ToString());
            */

            /*
                derp = new GameObject();
                derp.transform.position = pilot.transform.position;
                derp.transform.rotation = pilot.transform.rotation;
                MeshFilter mf = derp.AddComponent<MeshFilter>();
                MeshRenderer mr = derp.AddComponent<MeshRenderer>();
                mf.sharedMesh = p.m_mesh;
                if (mf.mesh != null) { 
                    Debugger.Log("I was able to create some kind of mesh bro");
                }
                mr.sharedMaterial = p.m_material;
                Debugger.Log(p.gameObject.transform.position.ToString());
            */

            return model;
        }

        /**
         * assetName is either the Steam workshop folder ID or the save name of the asset .crp file (not "name"). Do not include .crp
         * succ indicates whether both the mesh and material loads were successful
         * model is either an existing object to load the data into, or, if null, it'll return a new root-level one
         * debugOutput set to true to have it output verbose debugging information to Debugger.Log(...)
         **/
        public static GameObject LoadModel(string assetName, out bool succ, GameObject model = null, bool debugOutput = false) { 
            succ = false;
            bool modelSucc = false;
            bool materialSucc = false;
            MeshRenderer mr = null;

            if (debugOutput) if (debugOutput) Debugger.Log("Attempting to load asset " + assetName);
            foreach (Package p in PackageManager.allPackages) { 
                if (debugOutput) Debugger.Log("    Looking at asset " + p.packageName);
                if (debugOutput) Debugger.Log("    Looking at packageMainAsset " + p.packageMainAsset);
                if (p.packageName == assetName || p.packageMainAsset == assetName) { 
                    if (debugOutput) Debugger.Log("    Found package");
                    foreach (Package.Asset asset in p.FilterAssets(Package.AssetType.StaticMesh)) { 
                        if (debugOutput) Debugger.Log("    Found static mesh. Size: " + asset.size.ToString());
                        if (model == null) { 
                            model = new GameObject();
                        }
                        MeshFilter mf = model.AddComponent<MeshFilter>();
                        mr = model.AddComponent<MeshRenderer>();

                        mf.mesh = asset.Instantiate<Mesh>();
                        if (mf.mesh == null) { 
                            if (debugOutput) Debugger.Log("    Encountered an error while trying to load the mesh");
                            return null;
                        }
                        //mr.material = new Material(Shader.Find("Diffuse"));
                        modelSucc = true;
                        break;
                    }
                    foreach (Package.Asset asset in p.FilterAssets(Package.AssetType.Material)) { 
                        if (debugOutput) Debugger.Log("    Found material. Size: " + asset.size.ToString());
                        mr.sharedMaterial = asset.Instantiate<Material>();
                        materialSucc = true;
                        break;
                    }
                }
                if (modelSucc && materialSucc) { 
                    succ = true;
                    break;
                }
            }

            if (!modelSucc) { 
                if (debugOutput) Debugger.Log("    Loader was unable to find a model for " + assetName);
            }
            if (!materialSucc) { 
                if (debugOutput) Debugger.Log("    Loader was unable to find a material for " + assetName);
            }

            if (debugOutput) Debugger.Log("Load complete");
            return model;
        }
    }
}
