﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using ColossalFramework;

namespace CityCopter {
    public class CityCopterController : MonoBehaviour {
        float originalNearClipPlane, originalFieldOfView;

        float nearClipPlane = 0.05f;
        public bool modeEnabled = false;

        public Camera camera;
            PilotMouseLook mouseLook;

        public Transform originalCameraTransform;

        GameObject helicopter;
            GameObject pilot;
            GameObject rotor;
                GameObject rotorPlane;
            Light light;
            // These do async loading, possibly even from the web
            // It's up to the Controller's OWNER to initialize these!
            // Use GetAudioSource and GetHelicopterAudioController to access these
            Dictionary<HelicopterAudioType, HelicopterAudio> audioControllers = null;
            Dictionary<HelicopterAudioType, AudioSource> audioSources = null;
        HelicopterPhysics physics;

        // TODO: configuration option
        public const string HANGAR_NAME = "CityCopter Hangar 1";
        public enum HelicopterTypes { N916MU }; // default is N916MU

        public void init() { 
            bool loadSucc;
            Debugger.Log("CityCopter component initializing");

            helicopter = new GameObject();
            helicopter.name = "CityCopter Helicopter";
            helicopter.transform.parent = transform;
            CustomModelLoader.LoadModel("n916mu", out loadSucc, helicopter, false);

            pilot = new GameObject();
            pilot.transform.parent = helicopter.transform;
            pilot.transform.position = Config.Instance.pilotRelativePosition;

            rotor = new GameObject();
            rotor.transform.parent = helicopter.transform;
            rotor.transform.Translate(new Vector3(0f, 2.4048f, -0.2026f));
            CustomModelLoader.LoadModel("n916mu_main_rotor", out loadSucc, rotor, false);
            rotorPlane = GameObject.CreatePrimitive(PrimitiveType.Plane);

            physics = gameObject.AddComponent<HelicopterPhysics>();
            physics.helicopter = helicopter;
            physics.mainRotor = rotor;
            physics.blurRotor = rotorPlane;
            physics.init(HelicopterTypes.N916MU);

            GameObject lightOwner = new GameObject();
            light = lightOwner.AddComponent<Light>();
            light.type = LightType.Spot;
            light.range = 300;
            light.spotAngle = 40;
            light.transform.Rotate(45, -45, 0);
            light.intensity = 5;
            lightOwner.transform.parent = helicopter.transform;
            lightOwner.transform.Translate(new Vector3(0, 0, -5f));
            light.enabled = Config.Instance.useSpotlight;

            audioControllers = new Dictionary<HelicopterAudioType,HelicopterAudio>();
            audioSources = new Dictionary<HelicopterAudioType,AudioSource>();
        }

        public void SetNightMode(bool nightMode) { 
            GameObject dl = GameObject.Find("Directional Light");
            if (dl) { 
                dl.GetComponent<Light>().enabled = !nightMode;
            }
        }

        public void SetSpotlight(bool enabled) { 
            light.enabled = enabled;
        }

        public void setFieldOfView(float fov) { 
            Config.Instance.fieldOfView = fov;
            Config.Instance.Save();
            if (modeEnabled) {
                camera.fieldOfView = fov;
            }
        }

        /**
         * Returns true if it was possible to enable the mode
         **/
        public bool setMode(bool mode) { 
            bool succ = false;
            if (mode && !succ) modeEnabled = false;
            if (mode) { 
                Building hangarBuilding;
                Debugger.Log("Looking for the hangar");
                if (hasHangar(out hangarBuilding)) { 
                    originalFieldOfView = camera.fieldOfView;
                    originalNearClipPlane = camera.nearClipPlane;
                    camera.fieldOfView = Config.Instance.fieldOfView;
                    camera.nearClipPlane = nearClipPlane;

                    camera.transform.position = pilot.transform.position;
                    camera.transform.rotation = pilot.transform.rotation;
                    camera.transform.parent = pilot.transform;

                    mouseLook = camera.gameObject.AddComponent<PilotMouseLook>();
                    mouseLook.lockCursor = Config.Instance.lockMouse;
                    mouseLook.enabled = Config.Instance.mouseLook;

                    transform.position = hangarBuilding.m_position;
                    transform.rotation = Quaternion.Euler(Vector3.zero);
                    transform.Translate(new Vector3(0f, 0f, 4f));
                    SetNightMode(Config.Instance.nightMode);
                    SetSpotlight(Config.Instance.useSpotlight);


                    physics.resetPhysics();

                    SimplePingBack.SendStartPing();

                    succ = true;
                } else { 
                    Debugger.Log("Couldn't find the hangar!");
                }
            } else { 
                camera.fieldOfView = originalFieldOfView;
                camera.nearClipPlane = originalNearClipPlane;
                camera.transform.position = originalCameraTransform.position;
                camera.transform.rotation = originalCameraTransform.rotation;
                camera.transform.parent = null;

                SetNightMode(false);
                SetSpotlight(false);
                Destroy(mouseLook);
                succ = true;
            }
            modeEnabled = mode;

            return succ;
        }

        /** 
         * Has the user built a hangar?
         **/
        public bool hasHangar(out Building hangarBuilding) { 
            bool found = false;

            BuildingManager buildingManager = Singleton<BuildingManager>.instance;
            hangarBuilding = buildingManager.m_buildings.m_buffer[0]; // non-nullable type

            Building[] buildings = buildingManager.m_buildings.m_buffer;
            HashSet<string> seenIt = new HashSet<string>();
            for (int x=0; x<buildings.Length; ++x) { 
                if (buildings[x].Info == null || buildings[x].Info.name == null) continue;
                string name = buildings[x].Info.name;
                // Multiple hangars, with one disabled, etc?
                if (!seenIt.Contains(name) || (seenIt.Contains(name) && name.Contains(HANGAR_NAME))) { 
                    //Debugger.Log("Looking at a building named " + name + ". Flags are: " + buildings[x].m_flags.ToString());
                    if (!seenIt.Contains(name)) { 
                        seenIt.Add(name);
                    }
                    buildings[x].m_fireHazard = 255;

                    // Fuck this stupid non-int-casting flag bullshit. 
                    string flagString = buildings[x].m_flags.ToString();
                    if (name.Contains(HANGAR_NAME) && flagString.Contains("Created") && flagString.Contains("Active")
                        && !flagString.Contains("Demolishing") && !flagString.Contains("Deleted") && !flagString.Contains("Abandoned") 
                        && !flagString.Contains("Burned Down")){  // I blame the devs for not making this bitmask compatible
                        hangarBuilding = buildings[x];
                        Debugger.Log("5");
                        found = true;
                        break;
                    }
                }
            }

            return found;
        }

        Shader[] shaders;
        public void addRotorBlurObject(Texture2D rotorBlurAsset) { 
            if (rotorBlurAsset.GetPixels().Length <= 0 || rotorPlane == null || rotor == null) return; 
            rotorPlane.transform.parent = rotor.transform;
            rotorPlane.transform.position = rotor.transform.position;
            rotorPlane.transform.rotation = rotor.transform.rotation;
            rotorPlane.transform.Rotate(180f, 0f, 0f);
            // TODO: Configuration option
            rotorPlane.transform.Translate(new Vector3(0, -0.75f, 0f));
            rotorPlane.transform.localScale = rotor.transform.localScale;
            MeshCollider mc = rotorPlane.GetComponent<MeshCollider>();
            if (mc) { 
                Debugger.Log("c");
                mc.enabled = false;
            }
            MeshRenderer mr = rotorPlane.GetComponent<MeshRenderer>();
            if (mr) { 
                //mr..mainTexture = rotorBlurAsset;
                //mr.material = new Material(Shader.Find("Transparent/Diffuse"));
                Material m = new Material(Shader.Find("Unlit/Transparent"));
                //mr.material.mainTexture = rotorBlurAsset;
                mr.material = m;
                mr.material.mainTexture = rotorBlurAsset;
                //rotorPlane.GetComponent<Renderer>().material = m;
/*                shaders = new Shader[10];
                shaders[0] = Shader.Find("Transparent/Bumped Diffuse");
                if (shaders[0] != null)  Debugger.Log("    0");
                shaders[1] = Shader.Find("Transparent/Bumped Specular");
                if (shaders[1] != null)  Debugger.Log("    1");
                shaders[2] = Shader.Find("Transparent/VertexLit");
                if (shaders[2] != null)  Debugger.Log("    2");
                shaders[3] = Shader.Find("HeatDistort");
                if (shaders[3] != null)  Debugger.Log("    3");
                shaders[4] = Shader.Find("FX/Flare");
                if (shaders[4] != null)  Debugger.Log("    4");
                shaders[5] = Shader.Find("Particles/Additive");
                if (shaders[5] != null)  Debugger.Log("    5");
                shaders[6] = Shader.Find("Particles/Soft");
                if (shaders[6] != null)  Debugger.Log("    6");
                shaders[7] = Shader.Find("Particles/Multiply");
                if (shaders[7] != null)  Debugger.Log("    7");
                shaders[8] = Shader.Find("Particles/Multiply (Double)");
                if (shaders[8] != null)  Debugger.Log("    8");
                shaders[9] = Shader.Find("Transparent/Diffuse");
                if (shaders[9] != null)  Debugger.Log("    9");*/
            }

            physics.blurRotor = rotorPlane;

        }

        public AudioSource GetAudioSource(HelicopterAudioType audioType) { 
            AudioSource audioSource;
            if (!audioSources.TryGetValue(audioType, out audioSource)) { 
                audioSource = helicopter.AddComponent<AudioSource>();
                audioSources[audioType] = audioSource;
                if (audioType == HelicopterAudioType.ROTOR) { 
                    physics.audio = audioSource;
                }
            }

            return audioSource;
        }

        public HelicopterAudio GetHelicopterAudioController(HelicopterAudioType audioType) { 
            HelicopterAudio controller;
            if (!audioControllers.TryGetValue(audioType, out controller)) { 
                controller = rotor.AddComponent<HelicopterAudio>();
                audioControllers[audioType] = controller;
            }


            return controller;
        }

        public void Unloading() { 
            Debugger.Log("CityCopter component unloading");
            physics.shutdown();
            Destroy(pilot);
            Destroy(helicopter);
            Destroy(physics);
        }

        void Update() { 
            if (!Config.Instance.useDebugKeys) return;
            bool moveKey = false;
            bool posMoveKey = false;
            bool cameraKey = false;
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha1)) {
                Debugger.Log(physics.transform.eulerAngles.ToString());
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Alpha2)) {
                Debugger.ClearLog();
            }

            if (!Input.GetKey(KeyCode.LeftControl)) { 
/*                if (Input.GetKey(KeyCode.A)) { 
                    transform.Translate(new Vector3(-1.0f,0,0));
                    posMoveKey = true;
                }
                if (Input.GetKey(KeyCode.D)) {
                    transform.Translate(new Vector3(1.0f,0,0));
                    posMoveKey = true;
                }
                if (Input.GetKey(KeyCode.Q)) {
                    transform.Translate(new Vector3(0.0f,1.0f,0));
                    posMoveKey = true;
                }
                if (Input.GetKey(KeyCode.E)) {
                    transform.Translate(new Vector3(0.0f,-1.0f,0));
                    posMoveKey = true;
                }
                if (Input.GetKey(KeyCode.S)) {
                    transform.Translate(new Vector3(0.0f,0f,-1.0f));
                    posMoveKey = true;
                }

                if (Input.GetKey(KeyCode.W)) {
                    transform.Translate(new Vector3(0.0f,0f,1.0f));
                    posMoveKey = true;
                }
                */
            } else { 
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.A)) { 
                    camera.transform.Translate(new Vector3(-0.05f,0,0));
                    cameraKey = true;
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.D)) {
                    camera.transform.Translate(new Vector3(0.05f,0,0));
                    cameraKey = true;
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.Q)) {
                    camera.transform.Translate(new Vector3(0.0f,0.05f,0));
                    cameraKey = true;
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.E)) {
                    camera.transform.Translate(new Vector3(0.0f,-0.05f,0));
                    cameraKey = true;
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.S)) {
                    camera.transform.Translate(new Vector3(0.0f,0f,-0.05f));
                    cameraKey = true;
                }
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.W)) {
                    camera.transform.Translate(new Vector3(0.0f,0f,0.05f));
                    cameraKey = true;
                }
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.H)) { 
                light.transform.Rotate(0, 1.0f, 0);
                moveKey = true;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.L)) {
                light.transform.Rotate(0, -1.0f, 0);
                moveKey = true;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.K)) {
                light.transform.Rotate(1f, 0f, 0);
                moveKey = true;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.J)) {
                light.transform.Rotate(-1f, 0f, 0);
                moveKey = true;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.Comma)) {
                light.transform.Rotate(0, 0f, 1f);
                moveKey = true;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.Period)) {
                light.transform.Rotate(0, 0f, -1f);
                moveKey = true;
            }
            if (moveKey) { 
                //Debugger.Log("rotor position is now: " + rotor.transform.localPosition.ToString("F4"));
                Event.current.Use();
            }

            if (cameraKey) { 
                Event.current.Use();
                //Debugger.Log("Camera relative position is now: " + camera.transform.localPosition.ToString("F4"));
            }

            if ( moveKey) { 
               // Debugger.Log("Position is now: " + transform.position.ToString("F4"));
            }

        }
    }
}
