﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;

namespace CityCopter {
    public class Config {
        private static Config _instance;

        public float fieldOfView = 90f;
        public float nearClipPlane = 0.05f;
        public const string HANGAR_NAME = "CityCopter Hangar 1";
        private const string configPath = "CityCopterAlpha.xml";
//        public Vector3 pilotRelativePosition = new Vector3(0.6019f, 1.8577f, 1.6848f);
        public Vector3 pilotRelativePosition = new Vector3(-0.6019f, 1.8577f, 1.6848f);
        public KeyCode toggleHelicopterFlightKey = KeyCode.G;
        public EventModifiers mouseLookModifierKey = EventModifiers.Shift;
        public EventModifiers toggleHelicopterFlightKeyModifiers = EventModifiers.Control;
        public bool buildingCollisionDetection = true;
        public bool terrainCollisionDetection = true;
        public bool lockMouse = true;
        public bool mouseLook = true;
        public bool debugConsoleEnabled = false;
        public bool useSpotlight = false;
        public bool nightMode = false;
        public string logFileName = "CityCopterAlpha.log";
        public string readmeFileName = "CityCopterAlpha_README.txt";
        public bool truncateLogFile = false;
        public int rotorBlurWidth = 512;
        public int rotorBlurHeight = 512;
        public string EMBEDDED_ASSET_ROTOR_BLUR_512_PNG = "CityCopterAlpha.EmbeddedAssets.CityCopter_rotor_blur_512.png";
        public string EMBEDDED_ASSET_ROTOR_SOUND_WAV = "CityCopterAlpha.EmbeddedAssets.CityCopter_Rotor.wav";
        public string EMBEDDED_ASSET_ROTOR_SOUND_OGG = "CityCopterAlpha.EmbeddedAssets.CityCopter_Rotor.ogg";
        public string EMBEDDED_ASSET_README_TXT = "CityCopterAlpha.EmbeddedAssets.CityCopter_README.txt";
        public string ADDONS_DISK_LOCATION_FORWARD_SLASHES = "Colossal Order/Cities_Skylines/Addons";
        public string ASSET_FOLDER_NAME = "Assets";
        public string ASSET_ROTOR_FILE_NAME = "CityCopter_Rotor.ogg";
        public string README_DISK_LOCATION_APPDATA_FORWARD_SLASHES = "Colossal Order/Cities_Skylines/Addons/Assets/CityCopter_README.txt";
        public string README_FILENAME = "CityCopter_README.txt";
        public bool anonPingServerOnStart = false; //defaulting to opt-in rather than opt-out
        public string anonUUID = "";
        public bool invertYAxis = true;
        public bool flightSafeguards = true;
        public bool useDebugKeys = true;
        public bool enforceAltitudeLimit = true;

        public static Config Instance { 
            get { 
                if (_instance == null) {
                    _instance = Config.Deserialize(configPath);
                    if (_instance == null) { 
                        _instance = new Config();
                    }
                    _instance.Save();
                }

                return _instance;
            }
        }

        public void OnPreSerialize() {}
        public void OnPostDeserialize() {}

        public void Save() { 
            Config.Serialize(configPath, _instance);
        }

        // From FPS Camera Mod
        public static void Serialize(string filename, Config config)
        {
            var serializer = new XmlSerializer(typeof(Config));

            using (var writer = new StreamWriter(filename))
            {
                config.OnPreSerialize();
                serializer.Serialize(writer, config);
            }
        }

        public static Config Deserialize(string filename)
        {
            var serializer = new XmlSerializer(typeof(Config));

            try
            {
                using (var reader = new StreamReader(filename))
                {
                    var config = (Config)serializer.Deserialize(reader);
                    config.OnPostDeserialize();
                    return config;
                }
            }
            catch { }

            return null;
        }
    }
}
