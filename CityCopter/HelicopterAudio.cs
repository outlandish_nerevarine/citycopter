﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.IO;

namespace CityCopter {
    public enum HelicopterAudioType { ROTOR, RADIO, TAIL_ROTOR, WEAPON1, WEAPON2, WEAPON3, BASKET, CRASH, EXPLOSION };

    public class HelicopterAudio : MonoBehaviour {
        public static IEnumerator loadClipIntoSource(AudioSource audioSource, string fullPath = "") {
            //path = "file://" + Application.dataPath + "/../Files/Audio/" + Config.Instance.mainRotorAudioFilename; // works
            fullPath = "file://" + fullPath;
            Debugger.Log("Trying to load " + fullPath);
            WWW www = new WWW(fullPath);
            while (!www.isDone) {
               yield return null;
            }

            if (www.error == null) {
                audioSource.clip = www.GetAudioClip(true);
                //audioSource.volume = 1.0f;
                //audioSource.Stop();
                //audioSource.loop = true;
                //Debugger.Log("Finished audio loading attempt. Length is: " + audioClip.length);
                Debugger.Log("Finished audio loading attempt. Length is: " + audioSource.clip.length);
            } else { 
                Debugger.Log("Encountered an error when trying to load audio clip!");
            }
        }

    }
}
