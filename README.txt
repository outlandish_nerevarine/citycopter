This is CityCopter Alpha, a mod for Cities: Skylines. See the CityCopter_README.txt for more information.

If you just want to install the mod, download the latest version in the bin/ folder and follow the manual installation instructions in the README.txt.
