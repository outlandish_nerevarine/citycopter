﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.IO;

// From FPSCamera mod

namespace CityCopter {
    public class Debugger {
        private static Debugger instance = null;
        public DebugConsole console = null;
        public GameObject gameObject = null;
        private StreamWriter logFile;

        public static void Initialize() {
            instance = new Debugger();

            if (File.Exists(Config.Instance.logFileName)) {
                if (Config.Instance.truncateLogFile) { 
                    File.Delete(Config.Instance.logFileName);
                    instance.logFile = File.CreateText(Config.Instance.logFileName);
                } else { 
                    instance.logFile = File.AppendText(Config.Instance.logFileName);
                }
            } else { 
                    instance.logFile = File.CreateText(Config.Instance.logFileName);
            }
        }

        public static void Log(string s) {
            instance.LogInternal(s);
            if (Config.Instance.debugConsoleEnabled) { 
                instance.logFile.WriteLine(s);
                instance.logFile.Flush();
            }
        }

        public static void ClearLog() {
            instance.ClearLogInternal();
        }

        public void LogInternal(string s) {
            if (console == null) { 
                gameObject = new  GameObject();
                console = gameObject.AddComponent<DebugConsole>();
                console.debugger = this;
            } 
            if (Config.Instance.debugConsoleEnabled) { 
                console.Log(s);
            }
        }

        public void ClearLogInternal() { 
            if (console != null) { 
                console.text = "";
            }
        }

        public static void OpenConsole() { 
            instance.console.OpenConsole();
        }

        public static void CloseConsole() { 
            instance.console.CloseConsole();
        }

        public static void Shutdown() { 
            instance.logFile.Close();
        }

    }


    public class DebugConsole : MonoBehaviour {
        public Debugger debugger;
        private Rect windowRect = new Rect(16, 16, 380, 500);
        private Vector2 scrollPosition = Vector2.zero;
        public string text = "";
        private bool showConsole = false;

        private void OnGUI() {
            if (showConsole) {
                windowRect = GUI.Window(12522, windowRect, MainWindowFunc, "Debug Console");
            }
        }

        private void MainWindowFunc(int windowID) {
            GUI.DragWindow();
            scrollPosition = GUILayout.BeginScrollView(scrollPosition);
            GUILayout.Label(text);
            GUILayout.EndScrollView();
        }

        public void Log(string s) {
            text = String.Format("{0} * {1}\n", text, s);
        }

        void OnDestroy() {
            debugger.console = null;
        }

        public void OpenConsole() { 
            showConsole = true;
            debugger.LogInternal("Debug console opened");
        }

        public void CloseConsole() { 
            showConsole = false;
        }


        void Update() {
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.C)) {
                if (showConsole) {
                    CloseConsole();
                } else { 
                    OpenConsole();
                }
            }
        }
    }
}
