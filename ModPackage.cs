﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using ColossalFramework.Packaging;
using ColossalFramework;
using ColossalFramework.Math;
using System.Diagnostics;
using System.IO;

namespace CityCopter {
    // Handles the loading, unloading, configuration options, and toggling of the mod's mode
    class ModPackage : MonoBehaviour {
        // The mod package exists as a root level GameObject. 

        public static GameObject owner;
            public static ModPackage instance;
            public static GameObject child;
                public CityCopterController cityCopter;
                public CameraController controller;

        private bool modeEnabled = false;

        public CityCopterUI ui;

        public static void Initialize() { 
            Debugger.Log("Initializing mod package");

            owner = new GameObject();
            owner.name = Mod.MOD_NAME + "_owner";
            child = new GameObject();
            child.name = Mod.MOD_NAME + "_child";
            instance = owner.AddComponent<ModPackage>();
            instance.init();

            Debugger.Log("Finished init");
        }

        public void setMode(bool reqMode) { 
            // Request that the controller enables the mode (may not be possible)
            bool succ = cityCopter.setMode(reqMode);
            if (reqMode && succ) { 
                    controller.enabled = false;
                    cityCopter.transform.position = controller.transform.position;
                    cityCopter.transform.rotation = controller.transform.rotation;
                    modeEnabled = true;
                    child.SetActive(true);

            } else {
                controller.enabled = true;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                modeEnabled = false;
                child.SetActive(false);
            }

        }

        void Update() { 
            if (Event.current.type == EventType.KeyDown) { 
                if (Event.current.modifiers == Config.Instance.toggleHelicopterFlightKeyModifiers && Event.current.keyCode == Config.Instance.toggleHelicopterFlightKey) { 
                    if (!CityCopterUI.Instance.uiVisible || CityCopterUI.Instance.uiState == UIState.UI_STATE_NORMAL) { 
                        setMode(!modeEnabled);
                        if (modeEnabled) { 
                            CityCopterUI.Instance.Hide();
                        }
                    }
                }
            }

        }

        public void init() { 
            controller = GameObject.FindObjectOfType<CameraController>();
            cityCopter = child.AddComponent<CityCopterController>();
            cityCopter.originalCameraTransform = controller.transform;
            cityCopter.camera = controller.GetComponent<Camera>();
            cityCopter.init();

            initAudio(); 
        }

        private void initAudio() { 
            // The DLL contains an embedded OGG, which has to be saved locally and then loaded by the WWW class
            // If it exists on disk, it was saved previously. If not, get it from the DLL and save it for future use

            // Addons folder
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Config.Instance.ADDONS_DISK_LOCATION_FORWARD_SLASHES.Replace('/', System.IO.Path.DirectorySeparatorChar));
            bool useAssetsFolder = false;

            if (Directory.Exists(path)) { 
                path = Path.Combine(path, Config.Instance.ASSET_FOLDER_NAME);
                if (Directory.Exists(path)) {
                    Debugger.Log("Using the existing assets folder");
                    useAssetsFolder = true; // assets folder already exists
                } else { 
                    try {
                        Debugger.Log("The asssets folder doesn't exist. Creating it...");
                        Directory.CreateDirectory(path);
                        useAssetsFolder = true;
                    } catch (Exception e) { 
                        Debugger.Log("An exception occured while trying to create the Asset directory! " + e.ToString());
                    }
                }
            }

            if (!useAssetsFolder) { 
                path = ""; // will put the asset next to the EXE instead of in the assets folder, as that process failed
                Debugger.Log("Unable to get or create the addons or assets folders. Defaulting to using the folder that contains the exe, log, and xml.");
            }
            path = Path.Combine(path, Config.Instance.ASSET_ROTOR_FILE_NAME);
            ResourceLoader.WriteEmbeddedAssetToDisk(Config.Instance.EMBEDDED_ASSET_ROTOR_SOUND_OGG, path);

            AudioSource audioSource = cityCopter.GetAudioSource(HelicopterAudioType.ROTOR);
            StartCoroutine(HelicopterAudio.loadClipIntoSource(audioSource, path));
            audioSource.loop = true;

            Debugger.Log("Trying to load embedded rotor blur image");
            Texture2D texture = new Texture2D(Config.Instance.rotorBlurWidth, Config.Instance.rotorBlurHeight);
            Byte[] bytes = ResourceLoader.ExtractResource(Config.Instance.EMBEDDED_ASSET_ROTOR_BLUR_512_PNG);
            if (bytes == null) { 
                Debugger.Log("Error while trying to access embedded rotor image");
            } else { 
                Debugger.Log("Success. Bytes.Length: " + bytes.Length);
            }
            texture.LoadImage(bytes);
            cityCopter.addRotorBlurObject(texture);

            child.SetActive(false);

            ui = CityCopterUI.Instance;
        }


        public static void Unloading() { 
            Debugger.Log("Unloading mod package");

            instance.cityCopter.Unloading();
            UnityEngine.Object.Destroy(instance.cityCopter);
        }
    }
}
