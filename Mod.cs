﻿/*
CityCopter, a mod for Cities: Skylines
Copyright (C) 2015 InconsolableCellist

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICities;
using UnityEngine;
using System.Collections;
using System.Net;
using System.IO;

namespace CityCopter { 
    public class Mod : IUserMod {
        public const string MOD_NAME = "CityCopter Alpha";
        public const string MOD_VERSION = "0.1g";

        public string Name { 
            get { return MOD_NAME + " " + MOD_VERSION; }
        }

        public string Description { 
            get { return "Fly around your city in a helicopter"; } 
        }
    }

    public class ModLoader : LoadingExtensionBase {

        public override void OnLevelLoaded(LoadMode mode) {
            if (Config.Instance.anonUUID == "") { 
                Config.Instance.anonUUID = System.Guid.NewGuid().ToString();
            }
            Debugger.Initialize();
            Debugger.Log("Starting " + Mod.MOD_NAME + "  " + Mod.MOD_VERSION);
            Debugger.Log("Debug console loaded");
            ModPackage.Initialize();
            Debugger.CloseConsole();

            //SimplePingBack.SendLoadPing();
        }

        public override void OnLevelUnloading() {
            ModPackage.Unloading();
            Debugger.Shutdown();
            UnityEngine.Object.Destroy(ModPackage.owner);
            UnityEngine.Object.Destroy(ModPackage.instance);
        }
    }
}
