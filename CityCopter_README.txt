#Early Access: CityCopter Alpha 0.1e

##README

This is the early access version of CityCopter, called CityCopter Alpha! If all goes well, it should let you fly around (and into) your city in a helicopter, and, eventually, will have similar features to a certain Maxis game from back in the day.

Remember, this is an alpha; the helicopter model is ugly, and there *will* be bugs.

Visit https://www.reddit.com/r/CityCopter for more information!

##Installation:

CityCopter Alpha requires, at a minimum, the mods in this collection:

	CityCopter Alpha
	CityCopter Hangar 1
	CityCopter Alpha N916MU
	CityCopter Alpha N916MU Rotor

Install these mods, then start Cities and click Content Manager. 

    Click "Mods" on top. Find "CityCopter Alpha 0.1" in the list of mods and turn it on

    Click "Assets" on top. Make sure you see the hangar, n916mu, and n916mu_main_rotor in the list.

You can now start a new game or load an existing one. You should see a new, green settings button on the top right of your screen. Click it and check CityCopter's settings and change them as you like. 

Note that "anonymous ping" simply means that the mod will generate a random UUID and send it when you load the mod in a level, along with the version number you're running. This helps me see how many people are playing CityCopter and (in the future) verify that the Steam Workshop update process works.

##Playing:

Once the mod is running, you'll need to build a helicopter hangar before you can fly around. This is unlocked in the Level III unique buildings, and costs $30,000. You can use the "Unlock All" default mod to enable this early (and give me feedback if it should be easier to access). For now, place this hangar on level ground. If your helicopter gets stuck in the ground, try relocating it.

Once the hangar is built, use the "Enter Helicopter" keyboard shortcut to spawn your helicopter. The default key is Ctrl + G

* WASD will make you go up and down and roll left and right, respectively

* The mouse will turn you left and right, and pitch you up and down

* You can look around with the mouse while holding shift

Press the keyboard shortcut to stop flying. 

##Problems:

If you encounter any bugs please go to this thread and either upvote them or add it: https://www.reddit.com/r/CityCopter/comments/32nkip/bugs_known_bugs_for_alpha_testers/ 

##Configuration Options:

(Option - Default value - Description)

* Field of View - 90 - Set's the FOV on the pilot's camera 

* Hotkey to Enter Helicopter - Ctrl + G - Once you've built a hangar (and it's active and has power) use this hotkey to spawn your helicopter

* Building Collision Detection - True - Enables collision detection for all building types. May negatively impact performance on slower machines

* Terrain Collision Detection - True - Allows you to collide with the ground and roads. Has less of a performance impact than building collision detection

* Invert Y Axis - True - When disabled, mouse up/down causes your helicopter to pitch upwards/downwards, respectively

* Easier Flight - True - Toggles stability control, which makes it easier to keep your helicopter in proper orientations. If you disable this, the helicopter will allow you to make any control input you wish, but it makes it easier to get yourself into trouble

* Enable Logging - False - If you encounter issues and want to report a bug, definitely enable this. It outputs text to the log file, located as CityCopterAlpha.log inside of your Cities_Skylines folder in SteamApps/Common

* Use Spotlight - False - Toggles the experimental spotlight feature

* Night Mode - False - Toggles the experimental night more feature, which looks cool

* Use Debug Keys - True - Activates the following (temporary) key controls:

	. CTRL + W A S D Q E - Translates the camera inside and outside of the helicopter
	. CTRL + H J K L , . - Rotates the spotlight on its pivot

* Enforce Altitude Limit - True - Slows your helicopter down when you start to exceed its operational altitude. This makes it easier to fly in some cases, and in the future different helicopters will have different operational ceilings 

* Anonymous Ping on Start-up - True - Makes a simple HTTPS request when you start flight mode in the mod, which sends a random number associated with your mod as an ID (header "UUID"), a "START" header, and the version number of CityCopter that you're running. This information helps me determine what versions people have installed and how often the mod is activated, which is very useful information! 

Thanks for playing!
/u/InconsolableCellist
Temp helicopter model by easton
@MobiusStripShow

https://www.reddit.com/r/CityCopter
